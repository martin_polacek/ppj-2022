## Popiš anotace

#### @springbootapplication
- Spustí:
  - EnableAutoConfiguration
  - ComponentScan
  - Configuration

#### @EnableAutoConfiguration
- Povolí, aby Spring Boot nakonfiguroval aplikační kontext.
- Automaticky vytvoří a zaregistruje beany jak z načtených jar souborů, tak i beany nakonfigurované námi

#### @PropertySource
- Konfiguraci lze přesunout do properties souboru a následně ji pomocí této anotace načíst


#### @Profile
- Slouží pro spuštění aplikace v různých "módech"
- Např. při ladění může být použit profil DEV a využít lokální databázi, zatímco s profilem PROD může být použita produkční databáze

#### @value
- Slouží pro vkládání hodnot do jednotlivých beanů spravovaných Springem

## Popiš basic component z Logback logging system
- Logger - slouží pro výpis události na výstup
- Appender - jedná se o cíl logovacích událostí, může se jednat o konzoli nebo také komplexní databázi
- Layout - slouží k transformování určité informace o události do řetězce


## Popiš tři logovací levely s příkladem použití
- Error nastává pokud dojde k události, která ovlivní normální chod programu, ale aplikace je stále schopná pokračovat
- Info zpráva, která dává smysl koncovým uživatelům a adminsitrátorům, tzn. dává informaci o nějakém progresu aplikace
- Debug slouží pro detální informaci o chodbu aplikace, což využijí vývojáři aplikace při ladění aplikace


## Rozdíl mezi SL4F a Logback
- SL4F není logovací knihovna, ale slouží jako základní interface pro logovací knihovny (stejné používání)
- Logback jedná se o nástupce knihovny log4j a poskytuje několik vylepšení

## Popiš Grafana + Loki logging stack
- Grafana slouží pro analýzu a monitorování provozních dat
- Lze si zobrazovat jednotlivé data, metriky, protokoly schromážděné z aplikací v reálném čase pomocí přehledných grafů
- Lok je horizontálné škálovatelný agregační systém, který slouží pro ukládání logů a reagování na události

## Popiš následující výrazy ve spojení s application containerization
- Image - jedná se o read-only šablonu, která slouží pro build containerů
- Repository - jedná se o kolekci různých imagů se stejným jménem, ale jinými tagami
- Container je jedna instance vytvořená z image, může být vytvořeno mnoho containerů z jednoho image