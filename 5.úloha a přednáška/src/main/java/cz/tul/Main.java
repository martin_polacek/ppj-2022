package cz.tul;

import cz.tul.scan.SimpleComponentWithAutowiredConstructor;
import cz.tul.scan.SimpleComponentWithAutowiredSetter;
import cz.tul.service.GreetingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.Properties;

@SpringBootApplication
public class Main {


    public static void main(String[] args){
        SpringApplication.run(Main.class, args);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        Logger logger = LoggerFactory.getLogger(Main.class);

        ConfigurableEnvironment environment = context.getEnvironment();
        environment.setActiveProfiles( "DEV");
        logger.info("Nastaven DEV profil");


        Properties properties = new Properties();
        properties.setProperty("app.prop","test");
        environment.getPropertySources().addLast(new PropertiesPropertySource("external",properties));


        context.register(MainSpringConfiguration.class);
        context.refresh();

        GreetingService greetingService =  context.getBean(GreetingService.class);
        System.out.println(greetingService.greet("Ondra"));
        System.out.println("Property 'reldb.connection.url' is "+ environment.getProperty("reldb.connection.url"));

        context.getBean(SimpleComponentWithAutowiredConstructor.class).doSomething();
        context.getBean(SimpleComponentWithAutowiredSetter.class).doSomething();
        context.close();
    }
}
