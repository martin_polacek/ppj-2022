## Popiš dependency injection a inversion of control
- Při inicializaci programu jsou instance tříd vytvořeny pomocí kontajneru Springu na základě připravené konfigurace
- Tyto vytvořené instance jsou použity v místech programu, kde jsou deklarovány příslušné závislosti (Dependency injection)
- Proto inverze kontroly (Spring přebírá kontrolu nad vytvářením instancí tříd, podle dodané konfigurace)

## Popiš Spring application container
- definován v rámci balíku org.springframework
- úkolem je vytvořený kontajneru, jeho konfigurace a
- následné spojení jednotlivých komponent aplikace do jednoho funkčního celku

## Popiš anotace

#### @Autowired
- Implicitně vkládá závislost na objektu

#### @Component
- Používá se na třídy k indikování Spring componenty
- Označí třídu jako bean a přidá do aplikačního kontextu

#### @Configuration
- Používá se na třídy, které definují jednotlivé beans
```
  @Configuration
  public class DataConfig{ 
    @Bean
    public DataSource source(){
      DataSource source = new OracleDataSource();
      source.setURL();
      source.setUser();
      return source;
    }
    @Bean
    public PlatformTransactionManager manager(){
      PlatformTransactionManager manager = new BasicDataSourceTransactionManager();
      manager.setDataSource(source());
      return manager;
    }
  }
```
#### Qualifier
- Používá se v kombinaci s autowired
- Specifikuje přesnou závislost v případě, že existuje více než jeden bean se stejným typem
```
@Component
public class BeanA {
  @Autowired
  @Qualifier("beanB2")
  private BeanInterface dependency;
}
```

#### ComponentScan
- Používá se v kombinace s @Configuration
- Slouží k určení balíčku, který má být scanovát na anotované komponenty

## Co reprezentuje bean ve Springu

- objekt spravovaný frameworkem Spring
- vytvořený a řízený na základě konfigurace
  - třídu ke které náleží
  - jméno
  - rozsah platnosti (scope)
  - způsob inicializace
  - 

## Popiš rozdíl mezi Singleton a Prototype bean scope
- Singleton je vytvořena pouze jedna jediná instance pro celý Spring IoC
- Prototype = nová instance vźdy vytvořena při požadavku na daný bean

## Popiš bean lifecycle events
- Inicializace Beans
- Dependencies Injection
- Custom init() method
- Custom utility method
- Custom destroy() method