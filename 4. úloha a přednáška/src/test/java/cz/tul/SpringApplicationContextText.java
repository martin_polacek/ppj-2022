package cz.tul;

import cz.tul.service.GreetingService;
import cz.tul.service.impl.SimpleGreetingService;
import cz.tul.service.impl.YourBossGreetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Properties;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MainSpringConfiguration.class)
@ActiveProfiles({"DEV"})
public class SpringApplicationContextText {

    @Autowired
    ApplicationContext ctx;

    @Test
    public void testContextLoads() throws Exception {
        assertNotNull(this.ctx);
        assertTrue(this.ctx.containsBean("greetingService"));
    }

    @Test
    public void testAutowiredDependency() throws Exception {
        Object gs = this.ctx.getBean("greetingService");
        assertEquals(SimpleGreetingService.class, gs.getClass());
    }
}
