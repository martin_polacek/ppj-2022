package cz.tul.semestralni_prace;

import cz.tul.semestralni_prace.models.State;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.StateDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class StateTests {

    @Autowired
    StateDao sDao;

    @Autowired
    CityDao cDao;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void insertData() {
        sDao.deleteAll();
        cDao.deleteAll();
        State state1 = new State("CZ", "Czechia");
        State state2 = new State("DE", "Germany");
        sDao.insert(state1);
        sDao.insert(state2);
    }


    @Test
    void getAllStates_success() throws Exception {
        this.mockMvc.perform(get("/api/state/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[{\"code\":\"CZ\",\"name\":\"Czechia\"},{\"code\":\"DE\",\"name\":\"Germany\"}]")));
    }

    @Test
    void insertState_alreadyExists() throws Exception {

        this.mockMvc.perform(post("/api/state/insert")
                .param("code", "CZ")
                .param("name", "Czechia"))
                .andExpect(status().isConflict());
    }

    @Test
    void insertState_success() throws Exception {
        this.mockMvc.perform(post("/api/state/insert")
                        .param("code", "SK")
                        .param("name", "Slovakia"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteState_notFound() throws Exception {
        this.mockMvc.perform(delete("/api/state/delete")
                        .param("code", "SK"))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteState_success() throws Exception {
        this.mockMvc.perform(delete("/api/state/delete")
                        .param("code", "CZ"))
                .andExpect(status().isOk());
    }

    @Test
    void updateState_notFound() throws Exception {
        this.mockMvc.perform(put("/api/state/update")
                        .param("code", "SK")
                        .param("name", "Slovakiaa"))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateState_success() throws Exception {
        this.mockMvc.perform(put("/api/state/update")
                        .param("code", "CZ")
                        .param("name", "Czech republic"))
                .andExpect(status().isOk());
    }

}
