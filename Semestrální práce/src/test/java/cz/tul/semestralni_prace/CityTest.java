package cz.tul.semestralni_prace;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.State;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.StateDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class CityTest {

    @Autowired
    StateDao sDao;

    @Autowired
    CityDao cDao;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void insertData() {
        sDao.deleteAll();
        cDao.deleteAll();
        State state1 = new State("CZ", "Czechia");
        State state2 = new State("DE", "Germany");
        sDao.insert(state1);
        sDao.insert(state2);
        City city1 = new City(1L, "Liberec", "CZ");
        City city2 = new City(2L, "Praha", "CZ");
        cDao.insert(city1);
        cDao.insert(city2);
    }


    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void getAllCities_success() throws Exception {
        this.mockMvc.perform(get("/api/city/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[{\"id\":1,\"name\":\"Liberec\",\"state\":\"CZ\"},{\"id\":2,\"name\":\"Praha\",\"state\":\"CZ\"}]")));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void insertCity_alreadyExists() throws Exception {

        this.mockMvc.perform(post("/api/city/insert")
                .param("city", "Liberec")
                .param("state", "CZ"))
                .andExpect(status().isConflict());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void insertCity_success() throws Exception {
        this.mockMvc.perform(post("/api/city/insert")
                        .param("city", "Brno")
                        .param("state", "CZ"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteCity_notFound() throws Exception {
        this.mockMvc.perform(delete("/api/city/delete")
                        .param("id", "1000"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void deleteCity_success() throws Exception {
        this.mockMvc.perform(delete("/api/city/delete")
                        .param("id", "1"))
                .andExpect(status().isOk());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void updateCity_notFound() throws Exception {
        this.mockMvc.perform(put("/api/city/update")
                        .param("id", "1000")
                        .param("name", "Liberecc"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void updateCity_notFoundValidator() throws Exception {
        this.mockMvc.perform(put("/api/city/update")
                        .param("id", "1")
                        .param("name", "Liberecc123131"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void updateCity_success() throws Exception {
        this.mockMvc.perform(put("/api/city/update")
                        .param("id", "1")
                        .param("name", "Praha"))
                .andExpect(status().isOk());
    }

}
