package cz.tul.semestralni_prace;

import cz.tul.semestralni_prace.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

@SpringBootApplication
@EnableScheduling
public class SemestralniPraceApplication  implements ApplicationRunner {


    private static final Logger LOGGER= LoggerFactory.getLogger(SemestralniPraceApplication.class);

    @Autowired
    WeatherService weatherService;

    @Autowired
    private Environment environment;

    public static void main(String[] args)
    {

        SpringApplication app = new SpringApplication(SemestralniPraceApplication.class);
        app.run(args);

        LOGGER.info("Aplikace spuštěna!");

    }

    @Override
    @Profile(value = "!readOnly")
    public void run(ApplicationArguments args) throws Exception {
        String[] activeProfiles = environment.getActiveProfiles();

        if(Arrays.asList(activeProfiles).contains("readOnly")) {
            weatherService.updateWeatherData();
        }
    }

}

