package cz.tul.semestralni_prace.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.semestralni_prace.models.AvarageWeatherData;
import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.WeatherData;
import cz.tul.semestralni_prace.repository.CityDao;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
@EnableScheduling
@Service
@Profile("!readOnly")
public class WeatherService implements IWeatherService{

    private static final Logger LOGGER= LoggerFactory.getLogger(WeatherService.class);

    @Value("${openWeather.apiKey}")
    private String apiKey;

    @Value("${openWeather.URL}")
    private String apiURL;

    @Value("${openWeather.updateTime}")
    private int updateTime;

    @Autowired
    CityDao cityDao;

    @Autowired
    WeatherDataDao weatherDataDao;

    @Autowired
    WeatherService weatherService;

    private int calc() {
        List<City> cityList = cityDao.getAll();
        int duration  = (int)Math.floor(60 / cityList.size());
        int interval = 60/duration;
        if(interval < 10) interval = 10;
        if(updateTime > interval) interval = updateTime;
        return interval;
    }

    @Override
    public AvarageWeatherData avarageValues(Long cityId, int days) {
        Date dateBefore = calculateDate(days);
        return weatherDataDao.avarageValues(cityId, dateBefore);
    }

    @Override
    public AvarageWeatherData avarageValues(String state, int days) {
        Date dateBefore = calculateDate(days);
        List<City> citiesFromState = cityDao.get(state);
        ArrayList<Long> citiesId = new ArrayList<Long>();

        for(City c : citiesFromState) {
            citiesId.add(c.getId());
        }
        return weatherDataDao.avarageValues(citiesId, dateBefore);
    }

    private Date calculateDate(int days) {
        Date date = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -days);
        Date dateBefore = cal.getTime();
        return dateBefore;
    }

    @Override
    public WeatherData actualValue(Long city) {
        return weatherDataDao.actualValue(city);
    }

    @Override
    public List<WeatherData> dataForCity(Long city) {
        return weatherDataDao.dataForCity(city);
    }

    @Override
    public String csvDataForCity(Long city) {
        List<WeatherData> wData = weatherDataDao.dataForCity(city);
        String data = WeatherData.generateCSV(wData);
        return data;
    }

    @Override
    public WeatherData insert(WeatherData data) {
        return weatherDataDao.insert(data);
    }

    @Override
    public boolean importCSV(String data) {
        List<WeatherData> dataList = WeatherData.parseCSV(data);
        if(dataList.isEmpty()) return false;
        for(WeatherData d : dataList) {
            weatherDataDao.insert(d);
        }
        return true;
    }

    @Override
    public boolean removeValues(Long city) {
        weatherDataDao.removeValues(city);
        return true;
    }

    @Override
    public boolean checkDataExist(Long city) {
        WeatherData dataTest = weatherDataDao.actualValue(city);
        if(dataTest == null) {
            return false;
        }
        return true;
    }

    @Override
    @Async
    public void updateWeatherData() throws InterruptedException {
        List<City> cityList = cityDao.getAll();

        for(City city : cityList) {
            String url = String.format(apiURL, city.getName(), city.getState(), apiKey);
            WeatherData data = loadExternalData(city);
            weatherDataDao.insert(data);
        }
        LOGGER.info("Updating weather data");
        TimeUnit.SECONDS.sleep(calc());
        weatherService.updateWeatherData();


    }
    private WeatherData loadExternalData(City city) {
        RestTemplate restTemplate = new RestTemplate();
        String url = String.format(apiURL, city.getName(), city.getState(), apiKey);
        String response = restTemplate.getForObject(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        Document doc = Document.parse(response);

        Document mainData = (Document) doc.get("main");
        Document windData = (Document) doc.get("wind");
        Document cloudsData = (Document) doc.get("clouds");

        long city_id = city.getId();
        String state = city.getState();
        float temp = Float.parseFloat(mainData.get("temp").toString());
        float pressure = Float.parseFloat(mainData.get("pressure").toString());
        float humidity = Float.parseFloat(mainData.get("humidity").toString());
        float visibility = Float.parseFloat(doc.get("visibility").toString());
        float wind_speed  = Float.parseFloat(windData.get("speed").toString());
        float wind_deg = Float.parseFloat(windData.get("deg").toString());
        float clouds = Float.parseFloat(cloudsData.get("all").toString());

        WeatherData data = new WeatherData(city_id,state,temp,pressure,humidity,visibility,wind_speed,wind_deg,clouds);
        return data;
    }
}
