package cz.tul.semestralni_prace.service;

import cz.tul.semestralni_prace.models.AvarageWeatherData;
import cz.tul.semestralni_prace.models.WeatherData;

import java.util.List;

public interface IWeatherService {
    AvarageWeatherData avarageValues(Long cityId, int days);
    AvarageWeatherData avarageValues(String state, int days);
    WeatherData actualValue(Long city);
    List<WeatherData> dataForCity(Long city);

    String csvDataForCity(Long city);

    WeatherData insert(WeatherData data);

    boolean importCSV(String data);

    boolean removeValues(Long city);

    boolean checkDataExist(Long city);

    void updateWeatherData() throws InterruptedException;
}
