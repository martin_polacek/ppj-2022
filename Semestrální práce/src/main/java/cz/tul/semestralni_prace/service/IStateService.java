package cz.tul.semestralni_prace.service;

import cz.tul.semestralni_prace.models.State;

import java.util.List;

public interface IStateService {
    List<State> getAll();
    State get(String code);
    boolean insert(String city, String state);
    boolean delete(String code);
    boolean deleteAll();
    boolean update(String code, String name);
    boolean checkStateExist(String code);
}
