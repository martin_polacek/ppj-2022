package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.SemestralniPraceApplication;
import cz.tul.semestralni_prace.models.RestMessage;
import cz.tul.semestralni_prace.service.StateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/state")
@Profile("!readOnly")
public class RestStateController {

    @Autowired
    StateService stateService;

    private static final Logger LOGGER= LoggerFactory.getLogger(RestStateController.class);


    @PostMapping("/insert")
    public ResponseEntity insertState(@RequestParam("code") String code, @RequestParam("name") String name) {
        try {
            if(stateService.checkStateExist(code)) {
                RestMessage message = new RestMessage(409, "state with this code already exists");
                return new ResponseEntity<RestMessage>(message, HttpStatus.CONFLICT);
            }
            stateService.insert(name, code);
            RestMessage message = new RestMessage(200, "inserted");
            return new ResponseEntity(message, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity deleteState(@RequestParam("code") String code) {
        try {
            if(stateService.checkStateExist(code)) {
                stateService.delete(code);

                RestMessage message = new RestMessage(200, "deleted");
                return new ResponseEntity<RestMessage>(message, HttpStatus.OK);
            }
            else {
                LOGGER.warn("State not found for delete. Code: " + code);
                RestMessage message = new RestMessage(404, "state not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity updateState(@RequestParam("code") String code, @RequestParam("name") String name) {
        try {
            if(stateService.checkStateExist(code)) {
                stateService.update(code, name);
                RestMessage message = new RestMessage(200, "updated");
                return new ResponseEntity(message, HttpStatus.OK);
            }
            else {
                LOGGER.warn("State not found for delete. Code: " + code);
                RestMessage message = new RestMessage(404, "state not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(404, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }


}
