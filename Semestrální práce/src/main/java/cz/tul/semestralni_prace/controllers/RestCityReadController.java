package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.RestMessage;
import cz.tul.semestralni_prace.service.CityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/city")
public class RestCityReadController {

    @Autowired
    CityService cityService;

    private static final Logger LOGGER= LoggerFactory.getLogger(RestCityReadController.class);

    @GetMapping("/all")
    public ResponseEntity<List<City>> getAllCities() {
        try {
            return new ResponseEntity(cityService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(400, "unable to handle request");
            return new ResponseEntity(message, HttpStatus.BAD_REQUEST);
        }
    }

}
