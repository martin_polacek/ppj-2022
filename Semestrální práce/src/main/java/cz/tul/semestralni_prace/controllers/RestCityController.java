package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.models.City;
import cz.tul.semestralni_prace.models.RestMessage;
import cz.tul.semestralni_prace.service.CityService;
import cz.tul.semestralni_prace.service.StateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@org.springframework.web.bind.annotation.RestController
@Profile(value = "!readOnly")
@RequestMapping("/api/city")
public class RestCityController {

    @Autowired
    CityService cityService;

    @Autowired
    StateService stateService;

    private static final Logger LOGGER= LoggerFactory.getLogger(RestCityController.class);

    @PostMapping("/insert")
    public ResponseEntity insertCity(@RequestParam("city") String city, @RequestParam("state") String state) {
        try {
            City ct = new City(city, state);
            if(!cityService.validateCity(city, state)) {
                RestMessage message = new RestMessage(404, "city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
            if(!stateService.checkStateExist(state)) {
                RestMessage message = new RestMessage(404, "state not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
            if(!cityService.checkCityExist(city, state)) {
                cityService.insert(ct);
                RestMessage message = new RestMessage(200, "inserted");
                return new ResponseEntity<RestMessage>(message, HttpStatus.OK);
            }
            else {
                LOGGER.warn("city already exists: " + city);
                RestMessage message = new RestMessage(409, "city already exists");
                return new ResponseEntity<RestMessage>(message, HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(400, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity deleteState(@RequestParam("id") Long id) {
        try {
            if(cityService.checkCityExist(id)) {
                cityService.delete(id);
                RestMessage message = new RestMessage(200, "deleted");
                return new ResponseEntity<RestMessage>(message, HttpStatus.OK);
            }
            else {
                LOGGER.warn("City not found for delete: " + id.toString());
                RestMessage message = new RestMessage(404, "city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(400, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity updateName(@RequestParam("id") Long id, @RequestParam("name") String name) {
        try {

            if(cityService.checkCityExist(id)) {
                if(!cityService.validateCity(id,name)) {
                    RestMessage message = new RestMessage(404, "city does not exist");
                    return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
                }

                cityService.update(id, name);
                RestMessage message = new RestMessage(200, "updated");
                return new ResponseEntity<RestMessage>(message, HttpStatus.OK);
            }
            else {
                LOGGER.warn("City not found for update: " + id.toString());
                RestMessage message = new RestMessage(404, "city not found");
                return new ResponseEntity<RestMessage>(message, HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
            LOGGER.error(e.getMessage());
            RestMessage message = new RestMessage(400, "unable to handle request");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }


}
