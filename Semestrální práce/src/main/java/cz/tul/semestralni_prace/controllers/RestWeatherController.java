package cz.tul.semestralni_prace.controllers;

import cz.tul.semestralni_prace.SemestralniPraceApplication;
import cz.tul.semestralni_prace.models.RestMessage;
import cz.tul.semestralni_prace.repository.WeatherDataDao;
import cz.tul.semestralni_prace.models.WeatherData;
import cz.tul.semestralni_prace.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/weather")
@Profile("!readOnly")
public class RestWeatherController {

    private static final Logger LOGGER= LoggerFactory.getLogger(RestWeatherController.class);

    @Autowired
    WeatherService weatherService;

    @PostMapping("/import")
    public ResponseEntity importFile(@RequestParam("file") MultipartFile file) throws IOException {
        String csvFile = new String(file.getBytes());
        boolean imported = weatherService.importCSV(csvFile);

        if(!imported) {
            RestMessage message = new RestMessage(400, "unable to read CSV file");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        RestMessage message = new RestMessage(200, "imported");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
