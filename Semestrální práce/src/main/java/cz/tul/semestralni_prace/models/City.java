package cz.tul.semestralni_prace.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "city")

public class City {

    // Tutorial: https://zetcode.com/springboot/mysql/

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String state;

    public City() {}

    public City(String name, String state) {
        this.name = name;
        this.state = state;
    }

    public City(Long id, String name, String state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash * Objects.hashCode(this.id);
        hash = 79 * hash * Objects.hashCode(this.name);
        hash = 79 * hash * Objects.hashCode(this.state);
        return hash;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state=" + state +
                '}';
    }
}
