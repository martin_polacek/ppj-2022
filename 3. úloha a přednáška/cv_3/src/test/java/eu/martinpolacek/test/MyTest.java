package eu.martinpolacek.test;

import eu.martinpolacek.Arithmetic;
import org.junit.Assert;
import org.junit.Test;

public class MyTest {

    @Test
    public void addTest() {
        Arithmetic arithmetic = new Arithmetic();

        int result = arithmetic.add(10,20);
        Assert.assertEquals(30, result);
    }
    @Test
    public void subTest() {
        Arithmetic arithmetic = new Arithmetic();

        int result = arithmetic.sub(10,20);
        Assert.assertEquals(-10, result);
    }
}
