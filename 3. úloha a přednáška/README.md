## Rozdíl mezi namespace, module a service

- Namespace se používá k určení jednoznačného pojmenování prvků a dokumentů v POM 
- Modul jedná se o sub-projekt, který je součástí rodičovského Maven projektu.
- Service slouží k vytvoření interfacu, který poskytuje uživatelům vytvářet si vlastní pluginy.

## Popiš Meaven POM
- Jedná se o XML reprezentaci Maven projektu (konfigurační soubor) uloženou v souboru pom.xml
- Obsahuje hlavní informace pro to, aby šel vytvořit build projektu přesně tak, jak je požadováno.
- Obsahuje velké množství informací, hlavné s rozrůstrající se komplexitou projektu
- Popisuje závislosti, pluginy, goals, build profily, atd.

## Co je Super POM
- Jedná se o výchozí POM
- Všechny ostatní POM vycházejí ze Super POM
- I ta nejjednodušší forma POM tedy zdění všechny konfigurace definované v souboru Super POM

## Popiš Maven build životní cyklus
- validace = ověření zda je vše v projektu správně a všechny podstatné informace jsou k dispozici
- kompilace = kompilace zdrojového kódu projektu
- testování = testování pomoci unit testů
- package = převede kompilovaný kód a balíčky do distribučního formátu (např. JAR)
- integration-test = zpracuje a v případě potřeby použije balíček v prostředí, kde se používají integrační testy
- verify = spustí a zkontroluje, zda balíčky jsou validní a splňují kritéria
- install = nainstaluje balíčky do lokálního repozitáře, pro použití u jiných projektů lokálně
- deploy = zkopíruje finální package do vzdáleného repozitáře

## Popiš Maven goals
- Jedná se o cíle, které reprezentují speciální úkoly (menší než části lifecyclu), které složí při buildění a spravování projektu
- Můžou být přiřazeny k žádné nebo také k více fázím
- Goal, který neodpovídá žádné fázi, může být spuśtěn mimo životní cyklus přímým zavoláním

## How are project dependencies managed by Maven
- Jako závislost jsou myšlené jiné artefakty (projekty), které potřebuje projekt se své funkčnosti a kompilaci. Závislosti mají určený <strong>SCOPE</strong>, který určuje míru závilosti a okamžik její potřeby.
- Může nabývat hodnot:
  - compile = je vyžadováno pro kompilaci i běh aplikace
  - test = je vyžadováno pouze pro kompilaci a spuštění unit testůů
  - runtime = není vyžadováni při kompilaci, ale až za běhu
  - provided = je vyžadováno při běhu i kompilaci, ale bude poskytnuto až za běhu JVM
  - system = podobné jako provided, ale je nutné určit ručně cestu
