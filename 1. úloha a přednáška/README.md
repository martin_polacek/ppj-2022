# Přednáška

## Java

- Hashmap<Int,String>

Konstatní složitost O(1).

- ArrayList<String>

Dynamické pole, kde jednotlivé prvky na sebe vzájemně odkazují.

- Set<String>

Množina, ve které se může každý prvek vyskytovat jen jednou. Není uspořádaná.

- Rozdíl mezi abstraktní třídou a rozhraním

Abstraktní třída je třída, která nemá některé metody implementované. Očekává se, že třída, která z ní dědí si metody doplní.

Rozhraní je pouze seznam metod, které by měla třída podporovat. Pokud třída podporuje rozhraní, musí implementovat jeho všechny metody.


- Modifikátory přístupu
- Konstruktor
- Vnitřní třídy, final, override
- Java Concurrency–Thread, ThreadPool, Lock

## Web

- HTTP – (Hlavičky, stavové kódy)
- Statické X Dynamické web stránky
- Blokujicí, neblokující, asynchronní komunikace
- Webový x aplikační server, proxy
- HTML, CSS

HTML je základní značkovací jazyk pro strukturu webu. CSS se stará o vzhled webu.

- JSON, XML

Formáty pro výměnu dat. Hodně používané XML (má spoustu zbytečných značek), pokud známe strukturu je lepší využít JSON.

## DB

- Jak probíhá připojení k databázi
- Index, klíče, datové typy
- Kadinalita, parcialita

Kardinalita je počet vztahů, kterých se může entita účastnit. Parcialita udává, zda se entita musí/nemusí vztahu účastnit.

- Transakce

Skupina příkazů, která převádí databázi z jednoho konzistentního stavu do druhého. Buď se provede vše nebo nic. Při SELECTu i vnořené SELECTy dostávají stejná data.
Musí splňovat ACID.

# Cvičení

1) Popiš 5 novinek od verze 7

Local type interface, Records, Sealed classes, Switch Expressions 

```
int numLetters = switch (day) {
    case MONDAY, FRIDAY, SUNDAY -> 6;
    case TUESDAY                -> 7;
    case THURSDAY, SATURDAY     -> 8;
    case WEDNESDAY              -> 9;
};
```

TextBlock

```
String html = """
              <html>
                  <body>
                      <p>Hello, world</p>
                  </body>
              </html>
              """;
```

2) Co je local type interface?

Umožňuje přeskočit datový typ při deklaraci lokální proměnné. Ten je následně naznačen pomocí JDK a je na kompilátoru, aby zjistil konkrétní datový typ.

    var x = "Hi there";
    
    String x = " Hi there";

3) Popiš Java records (Java 15).

Jedná se o obdobu objektů (POJO), kdy record vytváří:
¨
- Veřejný konstruktor se všemi vnitřními proměnnými
- Všechny proměnné jsou nastavené jako private final
- Pro všechny proměnné jsou vytvořený veřejné gettery
- Vytváří toString, equals a hashCode metody
- Nelze dědit z jiného objektu
```
    import lombok.Value;

    @Value
    public class Osoba {
        String jmeno;
        String narodnost;
    }
```
4) Popiš Java sealed classes (Java 17)

Jedná se o třídy, kdy pomocí modifikátoru můžeme říct, které třídy jsou povolené, aby z této třídy/interfacu mohly dědit.

Lze také použít not-sealed, tím se určí, které dědit nemohou.

```
public abstract sealed class Vehicle permits Car, Truck {

    protected final String registrationNumber;

    public Vehicle(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

}
```
5) Rozdíl mezi Javou a JVM.

Java je programovací jazyk, který se následně zkompiluje na bytekód.

JVM je softwarový stroj, který vykonává instrukce (bytekód). V základu instrukce interpretuje, při častém použítí je kód zkompiloval do strojového kódu daného systému.

Jedná se o zásobníkový stroj. Pevně definované typy.
